#!/bin/bash -ex

# CONFIG
prefix="OfficeOpenXMLConverter"
suffix=""
munki_package_name="OfficeOpenXMLConverter"
display_name="Office Open XML Converter"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

mkdir OfficeOpenXMLConverter.mpkg

ditto "${mountpoint}/"*.mpkg OfficeOpenXMLConverter.mpkg

hdiutil detach "${mountpoint}"

## Unpack
mkdir build-root
(cd build-root; pax -rz -f ../OfficeOpenXMLConverter.mpkg/Contents/Packages/OpenXML_all_applications.pkg/Contents/Archive.pax.gz)

mkdir build-app
mkdir build-app/Applications

mv "build-root/"* build-app/Applications

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-app/Applications -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-app//' app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
